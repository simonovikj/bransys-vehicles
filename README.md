# vehicleform

## Server start
## ../Bransys/vehicleform/bransys-vehicle
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Start json server for reading data
```
json-server vehicles.json
```
